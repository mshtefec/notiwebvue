import { initializeApp } from 'firebase'

const app = initializeApp ({
    apiKey: "AIzaSyDd2ivlGl0xGx3jsQ_D_2Iyu_Ji0L6VaJU",
    authDomain: "notiweb-vue.firebaseapp.com",
    databaseURL: "https://notiweb-vue.firebaseio.com",
    projectId: "notiweb-vue",
    storageBucket: "notiweb-vue.appspot.com",
    messagingSenderId: "950009667153"
});

export const db = app.database();
export const noticesRef = db.ref('notices');