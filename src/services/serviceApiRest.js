//import { PUBLIC_KEY } from '../config.js'
const PUBLIC_KEY = 'eb123104167846d1801fe54c99ba16c0';
const BASE_URL_API = 'https://newsapi.org/v2/top-headlines?';

const getUrlApiSearch = query =>
  `${BASE_URL_API}sources=${query}&apikey=${PUBLIC_KEY}`

export const searchNotices = query => {
  const url = getUrlApiSearch(query)
  return fetch(url)
    .then(response => response.json())
    .then(({ data: { results } }) => results)
}