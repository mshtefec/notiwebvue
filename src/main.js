// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import './firebase';
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueFire from 'vuefire';
import Vue2Filters from 'vue2-filters'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
//import BalloonEditor from '@ckeditor/ckeditor5-build-balloon'
import VueCkeditor from 'vue-ckeditor5'

import firebase from 'firebase'

Vue.config.productionTip = false

const options = {
  editors: {
    classic: ClassicEditor
    //balloon: BalloonEditor
  },
  name: 'ckeditor'
}

Vue.use(VueCkeditor.plugin, options);

Vue.use(VueFire);
Vue.use(Vue2Filters);

let app;

firebase.auth().onAuthStateChanged(function(user) {
  if (!app) {
    /* eslint-disable no-new */
    app = new Vue({
      el: '#app',
      template: '<App/>',
      components: { App },
      router
    })
  }
});
