# Vue 2 + Firebase: How to build a Vue app with Firebase authentication system in 15 minutes

### How to Use

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```
